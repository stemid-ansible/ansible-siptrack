# Ansible siptrack playbooks

This relies primarily on my two roles called [siptrack-backend](https://gitlab.com/stemid-ansible/siptrack-backend) and [siptrack-frontend](https://gitlab.com/stemid-ansible/siptrack-frontend).

# Playbook run instruction

There is some configuration to be done, will write more here later.

Here is an example.

    $ ansible-playbook -e 'deploy_env=staging' -i hosts.inventory frontend.yml

1. bootstrap.yml
2. backend.yml
3. frontend.yml

There is also a database.yml that is optional because it only creates an empty DB and user, siptrack handles the schema creation.
